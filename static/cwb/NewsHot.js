var NewsHot = {
    'C':[
        {'id':'22704','record_date':'2020/06/01 18:52:44','title':'本局七股雷達站因  停機檢修，自2020年06月01日18時00分至2020年06月02日17時00分停止觀測','link':'#','target':'_blank'},
        {'id':'22702','record_date':'2020/06/01 12:13:24','title':'本局林園雷達站因檢查天線罩螺絲扭力，自2020年06月01日13時00分至2020年06月02日17時00分停止觀測','link':'#','target':'_blank'}
    ],
    'E':[
        {'id':'22705','record_date':'2020/06/01 18:52:47','title':'Radar RCCG will be stopped for the maintainance between 2020/06/01 18:00 and 2020/06/02 17:00.','link':'#','target':'_blank'},
        {'id':'22703','record_date':'2020/06/01 12:13:27','title':'Radar RCLY will be stopped for the maintainance between 2020/06/01 13:00 and 2020/06/02 17:00.','link':'#','target':'_blank'}
    ]
};

var Newsbb = {
    'C':[
        {'id':'2643','record_date':'2020/04/28 11:26:30','title':'梅雨5月正常至偏少，6月接近正常','link':'/V7/news/Newsbb/CH/20200428pressc.pdf','target':'_blank'},
        {'id':'2642','record_date':'2020/04/16 14:32:03','title':'地震海纜枋山登陸守家園','link':'/V7/news/Newsbb/CH/20200416press.pdf','target':'_blank'},
        {'id':'2641','record_date':'2020/03/06 15:13:45','title':'福衛七號觀測資料正式公開','link':'/V7/news/Newsbb/CH/20200306press.pdf','target':'_blank'},
        {'id':'2639','record_date':'2020/02/25 11:23:00','title':'春雨正常至偏少，氣溫偏暖','link':'/V7/news/Newsbb/CH/20200225.pdf','target':'_blank'},
        {'id':'2638','record_date':'2020/01/22 11:10:42','title':'~甘霖迎春鼠天意~春節連假天氣預報','link':'/V7/news/Newsbb/CH/20200122press.pdf','target':'_blank'},
        {'id':'2636','record_date':'2019/12/31 10:58:20','title':'108年臺灣均溫創新高','link':'/V7/news/Newsbb/CH/20191231press.pdf','target':'_blank'},
        {'id':'2635','record_date':'2019/12/27 16:48:39','title':'北部(樹林)防災降雨雷達啟用','link':'/V7/news/Newsbb/CH/20191227press.pdf','target':'_blank'},
        {'id':'2633','record_date':'2019/12/18 11:31:11','title':'震度新分級 應變更實用','link':'/V7/news/Newsbb/CH/1081218earthquakepress.pdf','target':'_blank'},
        {'id':'2625','record_date':'2019/12/05 16:17:50','title':'在最高學府 氣象與校園接軌','link':'/V7/news/Newsbb/CH/20191205press.pdf','target':'_blank'}
    ],
    'E':[
        {'id':'2644','record_date':'2020/04/28 11:30:51','title':'Mei-Yu Season Rainfall Expected to Be Below to Near Normal in May and Near Normal in June','link':'/V7/news/Newsbb/EN/20200428presse.pdf','target':'_blank'},
        {'id':'2640','record_date':'2020/02/25 11:30:29','title':'Come Spring, Rainfall Is Expected to Be Near to Below Normal and Temperature Above Normal ','link':'/V7/news/Newsbb/EN/20200225presse.pdf','target':'_blank'},
        {'id':'2637','record_date':'2019/12/31 11:03:30','title':'2019 on Track to Be the Hottest Year on Record','link':'/V7/news/Newsbb/EN/20191231presse.pdf','target':'_blank'},
        {'id':'2624','record_date':'2019/12/04 16:30:14','title':'Normal to Higher than Normal Temperature and Normal Rainfall Expected for This Winter','link':'/V7/news/Newsbb/EN/20191203presse.pdf','target':'_blank'}
    ]
};
var Notice = [
    {'no':'3044','keyin_time':'2020/05/20 08:57:17','info_name':'109年度法定預算-PDF格式','product_link':'notice_20200520085717.pdf'},
    {'no':'3043','keyin_time':'2020/05/20 08:56:16','info_name':'109年度法定預算-XML格式','product_link':'notice_20200520085616.zip'},
    {'no':'3042','keyin_time':'2020/05/08 15:09:16','info_name':'中央氣象局-前瞻特別預算109年4月份會計月報(XML)','product_link':'notice_20200508150916.zip'},
    {'no':'3041','keyin_time':'2020/05/08 15:08:44','info_name':'中央氣象局-前瞻特別預算109年4月份會計月報(PDF)','product_link':'notice_20200508150844.pdf'},
    {'no':'3040','keyin_time':'2020/05/08 15:08:04','info_name':'中央氣象局-單位預算109年4月份會計月報(XML)','product_link':'notice_20200508150804.zip'}
];
var ProspectivePlan = [
    {'id':'1','record_date':'2019/11/25 11:00:00','title':'海陸地震聯合觀測網計畫說明','link':'/V7/news/Upload/prospective_plan_20191125.pdf','target':'_blank'}
];