var Rt = '11137136913';
var WarnAll = ['W29','FIFOWS'];
var WarnTab = [];
var WarnHome = [];
var WarnFIFOWS = ['W26'];
var WarnContent = {
	'W26':{
		'C':{
			'title':'解除豪雨特報',
			'issued':'2020/06/01 21:40',
			'validto':'2020/06/02 05:44',
			'content':'發布時間：06/01 21:40\n\n由於降雨趨於緩和，發生大雨或豪雨的機率降低，故解除豪雨特報；今晚至明日臺灣北部及東半部地區仍有局部較大雨勢發生的機率。',
			'content_web':''
		},
		'E':{
			'title':'Dismiss Extremely Heavy Rain Advisory',
			'issued':'2020/06/01 21:40',
			'validto':'2020/06/02 05:44',
			'content':'Issued Time: 06/01 21:40\n\n1. Hualien County, \n\nHeavy Rain Advisory or Extremely Heavy Rain Advisory is cancelled tonight (06/01)\n\n2. Keelung North Coast, Taipei City Mountain Area, New Taipei City, Yilan County, \n\nHeavy Rain Advisory is cancelled tonight (06/01)\n\n',
			'content_web':''
		}
	},
	'W29':{
		'C':{
			'title':'高溫資訊',
			'issued':'2020/06/01 16:20',
			'validto':'2020/06/02 17:00',
			'content':'發布時間：06/01 16:20\n\n天氣晴朗炎熱，明(2)日中午前後花蓮縣縱谷為橙色燈號，有連續出現36度高溫的機率，請加強注意。\n\n避免非必要的戶外活動、勞動及運動，注意防曬、多補充水份、慎防熱傷害。室內保持通風及涼爽，建議採取人體或環境降溫的方法，如搧風或利用冰袋降溫等。關懷老人、小孩、慢性病人、肥胖、服用藥物者、弱勢族群、戶外工作或運動者，遠離高溫環境。',
			'content_web':''
		},
		'E':{
			'title':'Heat Information',
			'issued':'2020/06/01 16:20',
			'validto':'2020/06/02 17:00',
			'content':'Issued Time: 06/01 16:20\n\n1. Hualien County, \n\nOrange Signal (Daily maximum temperature reaches 36°C for three consecutive days) in effect from Tuesday morning (06/02) through Tuesday afternoon (06/02)\n\n ',
			'content_web':''
		}
	}
};
var WarnContent_W33 = [];
var WarnContent_TyWind = [];
var HEADER_VERSION = '20200526';
var FOOTER_VERSION = '20200505';
