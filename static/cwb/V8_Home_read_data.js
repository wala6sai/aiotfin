//Author: Bocheng
//Date: 2018/08/08
//首頁所有讀js, json資料的相關functions 都放在這裡.(PS: 讀天氣資料的程式在別的js檔案.)
//Jyunhua	20190314修改EN要多帶[VER]陣列[最新消息 & 新聞稿]

var gHomeTextTb={'more':{'C':'點我看更多','E':'More'}, 'less':{'C':'收合', 'E':'Less'},'noData':{'C':'無資料','E':'No Data'},
	'video':{'C':'影音專區','E':'Video'},'download':{'C':'點選以下載','E':'Download '}, 'format':{'C':'格式', 'E':' format'},
	'openNewWindow':{'C':'另開新視窗', 'E':'Open in new window'},
	'nextPage':{'C':'下一頁', 'E': 'Next'}, 'prevPage':{'C':'上一頁', 'E':'Previous'},
	'eqReport':{'C':'地震報告', 'E':'Earthquake Report'},
	'tyReport':{'C':'颱風報告', 'E':'Typhoon Report'},
	'w24Report':{'C':'大規模或劇烈豪雨報告','E':'Heavy Rain Report'}, //目前英文版沒有W24.
	'close':{'C':'關閉', 'E':'Close'},
	'note':{'C':'重要公告', 'E':'Important notice'},
	'more2':{'C':'更多內容：彈出畫面', 'E':'More: popup'},
	'click2check':{'C':'點此前往檢視', 'E':'Click to check details'},
	'goodSite':{'C': '好站介紹', 'E': 'Good website'},
	'noNews': {'C': ['目前尚無最新消息之資訊', '目前尚無發布新聞稿','目前無相關資訊','目前無討論話題','目前無熱門話題', '目前無相關資訊'], 'E': ['no announcement','no announcement']}
};

function loadVideoList(){

	var str="";
	var main=true;

	var $main=$('.video-main');
	var $sub=$('.video-sub');

	var MENU_MAX=6; //顯示清單的最大數量, 但應該要計算總字數再算出可顯示的最大數量或許較好.
	var count=0;

	for(var key in HomeVideoList){

		var obj=HomeVideoList[key];
		var title=obj['title'];	
		var url=obj['url'];
		//預設第一個放在左邊的main(有影片預覽), 其他放在右邊的sub(選單)
		str='<li><a title="'+title+'" href="'+url+'"><i class="icon-player" aria-hidden="true"></i>'+title+'</a></li>';
		$str=$(str);

		if(count>=MENU_MAX){
			$str.hide();
		}

		//這裡或許要限制清單的總數量, 以免將過多的清單塞到網頁, 展開會很難看.
		$sub.append($str);
	
		//ref : http://promincproductions.com/blog/youtube-image-thumbnail-urls/	
		if(main){
			url += '?rel=0';
			str = '<iframe class="width-100" height="300" title="'+gHomeTextTb['video'][VER]+'" src="' + url + '" allowfullscreen></iframe>';
			$main.html(str);
			main=false;
		}

		count++;
	}

	str='<div class="media-viewmore"><a title="' + gHomeTextTb['more'][VER] + '" href="javascript:void(0)" class="btn btn-default-r"><i class="fa fa-plus"></i>'+gHomeTextTb['more'][VER]+'</a><a title="' + gHomeTextTb['less'][VER] + '" href="javascript:void(0)" class="btn btn-default-r less"><i class="fa fa-minus"></i>'+gHomeTextTb['less'][VER]+'</a></div>';
	$sub.append(str);

	$('.media-viewmore').click(function(){
		$(this).siblings('li:gt('+ (MENU_MAX-1) +')').toggle().end().find('a').toggle();
	});

	$sub.find('li').on('click', function(){
		var url = $(this).children('a').prop('href');
		if(url.match(/youtube.com/)!==null){
			url += '?autoplay=1&rel=0';
		}
		var str = '<iframe class="width-100" height="300" title="'+gHomeTextTb['video'][VER]+'" src="' +url+ '" allowfullscreen></iframe>';
		$main.html(str)
		return false;

	});
}

//暫時的函數 START

//給新聞稿連結, 討論話題, 熱門話題使用
//它們只有給檔案名稱沒有給完整路徑
function getLink(title, link){

	//可能是連結, 直接返回.
	if(link.indexOf('http://')!==-1 || link.indexOf('https://')!==-1){
		return link;
	}

	if(link.match(/^\/V7\/.+\.htm/)){

		return 'https://www.cwb.gov.tw' + link;
	}

	if(title=='政府資訊公開連結'){
		link= '/Data/service/notice/download/' + link;

	}else if(title=='討論話題' || title=='熱門話題' || title == 'HotTopic'){
		link= '/Data/service/hottopic/' + link;

	}else if(title=='最新消息連結' || title=='前瞻計畫專區' || title == 'Event'){
		link=link.replace(/\/V7\/news\/Upload\//,'/Data/service/news/Upload/');


	}else if(title=='新聞稿連結' || title == 'News'){
		link=link.replace(/\/V7\/news\//, '/Data/service/');
	}

	return link;

}
//暫時的函數 END


//用於V8首頁的最新消息, 新聞稿, 政府資訊公開, 討論話題,熱門話題
//先引入 /Data/service/NewsHot.js,  
//NewsHot=>最新消息, 
//Newsbb=>新聞稿, 
//Notice=>政府資訊公開, 
//HotTopoc=>討論話題, 
//HotTalk=>熱門話題
//必須要放 <script type="text/javascript" src="/V8/assets/js/collapse.js"></script> 的上方
function loadHotNews(items){

	for(var i=0; i<items.length; i++){

		var item=items[i]['data'];
		var num=i+1;
		var ele='#tab-'+num;
		var $target=$(ele).find('.hiddenitem');
		var title_attr=items[i]['title'];
		
		//Jyunhua	20190314修改EN要多帶[VER]陣列[最新消息 & 新聞稿]
		if(title_attr=="Event" || title_attr=="News"){
			item=items[i]['data'][VER];
		}
		
		for (var j=0; j<item.length; j++){
			var obj=item[j];
			if(obj['link']==undefined){
				var link=obj['product_link'];
				link=getLink(title_attr, link);
			}else{
				var link=obj['link'];
				link=getLink(title_attr, link);
			}

			var a_openTag='';
			var a_closeTag='';
			if(link=='#'){
				link=''; //如果要按下沒反應, 也可以設定為 '#!'. 
				// 參考:https://stackoverflow.com/questions/13003044/href-going-to-top-of-page-prevent
			}else{
				a_openTag='<a href="' + link + '"';
				a_closeTag='</a>';
			}

			var record_date='';
			if(obj['pub_start']==undefined){
				if(obj['record_date']==undefined){
					record_date=obj['keyin_time'];
				}else{
					record_date=obj['record_date'];
				}
			}else{
				record_date=obj['pub_start'];
				record_date=record_date.replace(/-/g,'/');
				if(!record_date.match(/\s*\d+:\d+:\d+/)){
					record_date += ' 10:00:00'; //後面沒有時分秒, 預設給 10:00:00
				}
			}
	
			var date_str=record_date.replace(/\s*\d+:\d+:\d+/, '');

			if(obj['title']==undefined){
				var title=obj['info_name'];
			}else{
				var title=obj['title'];
			}

			if(obj['target']==undefined){	
				var target='_blank';
			}else{
				var target=obj['target'];
			}
			var a_target_attr='';
			if(link!='' && target!=''){

				var filename = link.split('/').pop();
				var ext = filename.split('.').pop();

				if(ext.match(/pdf|zip|doc|odf|odt|xls/)){

					a_target_attr=' target="'+target+'"';
					a_openTag +=  a_target_attr + ' title="' + gHomeTextTb['download'][VER] + title + '(' + ext + gHomeTextTb['format'][VER] + ')' + '">';	
				
				}else{
					a_target_attr=' target="'+target+'"';
					if(target=='_blank'){
						a_openTag +=  a_target_attr + ' title="' + title + '(' + gHomeTextTb['openNewWindow'][VER] + ')' + '">';	
					}else{
						a_openTag +=  a_target_attr + ' title="' + title + '">';
					}
				}
			}
			
			var newHtml;
			if(isNew(record_date)){
				newHtml='<span class="new">NEW</span>';
			}else{
				newHtml='';
			}
			var str='<li><div class="flex-row-nowrap align-start"><span class="date-mark">' + date_str + '</span>' + newHtml + a_openTag + '<span>'+title+'</span>'+ a_closeTag +'</div></li>';
			$target.append(str);
		}
	}
	
	$('.hiddenitem').each(function(_i){

		var max=4; //這裡必須是4, 不能改(改了也沒用). 因css已寫死顯示4個.
		if ($(this).children('li').length > max) {
			$(this).append('<div class="viewmore"><a title="' + gHomeTextTb['more'][VER] + '" href="javascript:void(0)" class="btn btn-default-r"><i class="fa fa-plus"></i>'+gHomeTextTb['more'][VER]+'</a><a title="' + gHomeTextTb['less'][VER] + '" href="javascript:void(0)" class="btn btn-default-r less"><i class="fa fa-minus"></i>'+gHomeTextTb['less'][VER]+'</a></div>');
			$('.viewmore', this).on('click', function() {
				$(this).siblings('li:gt(' + (max-1) + ')').toggle().end().find('a').toggle();
			});

		}else if ($(this).find('li').length == 0 ) {
			$(this).append('<div class="nomore"><a title="' + gHomeTextTb['noNews'][VER][_i] + '" href="javascript:void(0)" class="End">'+gHomeTextTb['noNews'][VER][_i]+'</a></div>');
		}	
	});

	// for accessibility.
	var gNewsId = null;
	$('div.index-news a').each(function(i){
		if(i!=0){
			$(this).attr('tabindex', -1);
		}
	}).focus(function(){
		
		var id = $(this).attr('href');
		gNewsId = id;
		$('div.index-news a').each(function(i){
			$(this).attr('tabindex', -1);
		});
		$(this).attr('tabindex', 0);
		$(this).click();

	}).keydown(function(e){

		if(e.shiftKey && e.which == 9){
			$('html,body').stop(true); // 這是因為 index-function.js 會跑動畫, 需要先停止, 否則會卡住.
			$('div.index-news a').each(function(i){
				$(this).attr('tabindex', 0);
			});
			
		}
	});

	$('div.tab-content').append('<div id="news-tab-point" tabindex="0"></div>');
	$('#news-tab-point').keydown(function(e){
		var num = parseInt(gNewsId.split('-')[1]);

		if(e.shiftKey && e.which == 9){
			if(num!=6){
				$('a[href="#tab-'+num+'"]').focus();
			}
			$('div.index-news a').each(function(i){
				$(this).attr('tabindex', 0);
			});

		}else if(e.which == 9){
			num++;
			$('a[href="#tab-'+num+'"]').focus();
		}
	});
}

//dataTime: string. expected format: YYYY/MM/DD HH:MM:SS
//Review: 需要如此麻煩嗎? 要再想一想
function isNew(dataTime){

	var MAX_HOURS=8;
	dateTime=dataTime.trim();
	//將時間都轉換成UTC(+00:00), 再做比較. 
	//UTC, GMT 不太一樣, 但仍可認為是一樣的
	dataTime=dataTime.replace(/\//g, '-');
	dataTime=dataTime.replace(/\s+/, 'T');
	dataTime+= '+08:00'; // 臺灣時區GMT+08:00
	dataTime=new Date(dataTime); // 告訴javascript 資料時間是GMT+08:00
	dataTime=dataTime.valueOf(); //轉換為 UTC (in ms), 即GMT+00:00

	//new Date() 返回的是client端的現在時間(local time, 會根據時區做增減).
	var client = new Date() ;	
	client = client.valueOf(); //轉換為 UTC (in ms), GMT+00:00

	//會有負值的情況發生嗎?
	var diff=client - dataTime; //in ms

	var hours= diff/1000/60/60; //in hours.
	//console.log(hours);
	if(hours <= MAX_HOURS){
		return true;
	}else{
		return false;
	}
}

//載入好站介紹
function loadGoodSites(){
	
	var str='';
	var obj;
	var thisGoodSites = GoodSites[VER];
	for(var i=0, len=thisGoodSites.length; i<len; i++){
		obj=thisGoodSites[i];
		str+= '<div class="item">'+
		'<a href="' + obj.href + '" arial-label="' + gHomeTextTb['goodSite'][VER] + '" title="' + obj.title + '(' + gHomeTextTb['openNewWindow'][VER] + ')" class="img-wrap" target="_blank">';
		if(obj.img){
			str+='<img src="' + obj.img + '" class="img-responsive"'+ ' alt="' + obj.title + '">';
		}else{
			str+='<span>' + obj.title + '</span>';
		}
		str+='</a></div>';
	}

	if(thisGoodSites.length == 0){
		var section = $('#good-sites-owl').parents('section');	
		section.hide();
		return ;
	}
	$('#good-sites-owl').html(str);
	$('.friend-carousel').owlCarousel({
		loop:false,
		rewind:true,
		margin:10,
		nav:false,
		responsive:{
		0:{
			items:1
		},
		600:{
			items:3
		},
		1000:{
			items:4
		}
		}
	});

	$('.friend-carousel > .owl-dots').css({'top':'110%'});

}

function loadImportantNews(news, ver){

        if(ver != 'C' && ver != 'E'){
                ver='C';
        }

        var html=buildNewsElement(news, ver);
        $('#header').after(html);
}

function buildNewsElement(news, ver){

        var str='<div class="main-title-bar" id="important-notice">'+
        '<div class="container default-flex">'+
        '<h2 class="main-title"><i class="fa fa-bullhorn"></i>'+gHomeTextTb['note'][VER]+'</h2>'+
        '<a href="#" aria-label="'+gHomeTextTb['note'][VER]+'" data-toggle="modal" data-target="#marquee" role="button" class="marquee red" title="'+gHomeTextTb['more2'][VER]+'">'+news+'<span><span>'+gHomeTextTb['more'][VER]+'</span><i class="fa fa-plus-circle"></i></span></a>'+
        '</div>'+
        '</div>'+
        '<div class="modal fade" id="marquee" tabindex="-1" role="dialog" aria-labelledby="marqueeLabel">'+
        '<div class="modal-dialog" role="document">'+
        '<div class="modal-content">'+
        '<div class="modal-body" id="marqueeLabel">'+
        '<p>'+news+'</p>'+
        '</div>'+
        '<div class="modal-footer">'+
        '<button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="'+gHomeTextTb['close'][VER]+'">'+gHomeTextTb['close'][VER]+'</button>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>';

        return str;
}

//載入公告
function loadHomeHeaderNews(){

        var news=HomeHeaderNews[VER];
        news=news.trim();
        if(news){
                loadImportantNews(news, VER);
        }
}

//src: string.
//id: string.
//obj: Example:  {'src':'/.../...', 'id':'#dis-1'}
function loadWarningContent(obj){

	var src=obj['src'];
	var id=obj['id'];
	var content=obj['content'];

	if(src !== undefined){

		$.ajax({url:src,cache:false}).done(function(data){

			$(id).html(data);

		}).fail(function(xhr, status, error){

			console.log('Loading ' + src + ' failed: ' + error);
		});	

	}else{	
		$(id).html(content);
	}

}

function loadWarning(){
	//可參考 /Data/js/info/Info_Warning.js
	//var VER='C';
	var tabHtml='';
	var contentHtml='<div class="tab-content">';
	var EQ_MOD_URL = '/V8/_LANG_VER_/ajax/_EQ.html';
	var warnObjs={
		
		'EQ':{'icon':'icon-taiwaneq'},
		'TY_WARN':{'icon':'icon-warn_typhoon'},
		'W24':{'icon':'icon-warn_thunder'}
	};

	var obj;
	var first=true;
	var no=1;
	var params=[];
	var tyInfoHtml='';
	var tyIds = [];

	for(var i=0, len=WarnHome.length; i<len; i++){

		warn=WarnHome[i];
		obj=warnObjs[warn];

		if(obj===undefined){ //沒定義, 無法找出對應class跟MOD網址, 跳過
			console.log('[Warning] the warn:' + warn + ' not defined.');
			continue;
		}

		//if(!WarnAll.includes(warn)){ // 必須 WarnAll 和 WarnHome 同時有才可繼續. ie 11 不支援 includes
		var theWarnExist=false;
		for(var w=0, wLen=WarnAll.length; w<wLen; w++){
			if(warn == WarnAll[w]){
				theWarnExist=true;
				break;
			}
		}
		if(!theWarnExist){
			console.log('[Warning] the warn: '+ warn + ' may go wrong.');
			continue;
		}

		if(warn == 'EQ'){
			
			var o={};
			o['id']='#dis-'+no;
			o['src']=EQ_MOD_URL.replace('_LANG_VER_', VER);


			if(first){
                        	tabHtml += '<li class="active cwb-eq">';
                        	contentHtml += '<div class="tab-pane fade in active" id="dis-' + no + '"></div>';
                        	first=false;
                	}else{
                        	tabHtml += '<li class="cwb-eq">';
                        	contentHtml += '<div class="tab-pane fade in" id="dis-' + no + '"></div>';
                	}

			tabHtml+='<a href="#dis-' + no + '" data-toggle="tab" aria-label="'+gHomeTextTb['eqReport'][VER]+'">'+
			'<i class="' + obj['icon'] + ' pull-left"></i>'+ WarnAllName[warn][VER] +
			'<i class="fa fa-caret-down pull-right hidden-xs"></i>'+
			'</a></li>';

			params.push(o);
			no++;
			

		}else if(warn == 'TY_WARN'){
			
			var tyData=TY_WARN_LIST[VER];
			var tyDataLen=tyData.length;
			var tyObj;

			var MovementHtml='';
			if(Movement.trim()){
				MovementHtml = '<p>' + Movement + '</p>';
			}

			var LandSeaWarnHtml='';
			//暫定, 英文名稱需再確認
			var LandSeaText={
				'land':{'C':'陸上', 'E':'Land'},
				'sea':{'C':'海上', 'E':'Sea'}
			};

			if(LandWarn.trim()){
				LandSeaWarnHtml += '<p>' + LandSeaText['land'][VER] + '：' + LandWarn + '</p>';
			}
			if(SeaWarn.trim()){
				LandSeaWarnHtml += '<p>' + LandSeaText['sea'][VER] + '：' + SeaWarn + '</p>';
			}
			
			var HeavyRainHtml='';
			if(HeavyRain.trim()){
				HeavyRainHtml = '<p>' + HeavyRain + '</p>';
			}

			var NoticeTextHtml='';
			for(var j=0, NoticeLen=NoticeText.length; j<NoticeLen; j++){
				if(NoticeText[j].trim()){
					NoticeTextHtml += '<li>' + NoticeText[j] + '</li>';
				}
			}
			if(NoticeTextHtml){
				NoticeTextHtml = '<ol>' + NoticeTextHtml + '</ol>';
			}
			
			var NoteTextHtml = '';
			for(var j=0, NoteLen = NoteText.length; j<NoteLen; j++){
				if(NoteText[j].trim()){
					NoteTextHtml += '<li>' + NoteText[j] + '</li>';
				}		
			}
			if(NoteTextHtml){
				NoteTextHtml = '<ol>' + NoteTextHtml + '</ol>'; 
			}

			var tyInfoContentObj={
				'C':[{'info':'颱風動態','html':MovementHtml}, {'info':'警戒區域及事項','html':LandSeaWarnHtml},
					{'info':'豪雨特報','html':HeavyRainHtml}, {'info':'注意事項','html':NoticeTextHtml},
					{'info':'附註', 'html': NoteTextHtml}], 
				'E':[]
			};

			var tyInfoContent='';
			var info;
			var infoHtml;
			for(var j=0, tyInfoLen=tyInfoContentObj[VER].length; j<tyInfoLen; j++){

				info=tyInfoContentObj[VER][j]['info'];
				infoHtml=tyInfoContentObj[VER][j]['html'];
				if(!infoHtml){
					continue;
				}

				tyInfoContent+='<div class="panel panel-default">'+
                        	'<div class="panel-heading">'+
                          	'<h4 class="panel-title">'+
                            	'<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-'+j+'" title="顯示'+info+'資訊" aria-expanded="false">'+info+'</a>'+
				'</h4>'+
				'</div>';
				if(j==0){
					tyInfoContent += '<div id="collapse-'+j+'" class="panel-collapse collapse in" aria-expanded="false" role="document">';
				}else{
					tyInfoContent += '<div id="collapse-'+j+'" class="panel-collapse collapse" aria-expanded="false" role="document">';
				}
				tyInfoContent += '<div class="panel-body">'+
				infoHtml+	
				'</div>'+
				'</div>'+
				'</div>';	
			}
	
			// 這裡的 tyInfoHtml 不寫在各颱風div內, 而是獨立出來到外面, 主要原因是因為在第二個颱風div以後點選該title, 
			// 會沒有反應, 已試過換ID還是一樣.	
			if(tyInfoContent){
				tyInfoHtml += '<div class="col-md-12 cwb-ty-info">' +
				'<div class="panel-group acc-v1 vision_1 margin-top-30" id="accordion-1">'+
				tyInfoContent+	
				'</div>'+
				'</div>';
			}
	
			var tabName, tabBody;
			for (var j=0, tyLen=tyData.length; j<tyLen; j++){

				var o={};
				o['id']='#dis-' + no;
				tyIds.push(no);
				tyObj=tyData[j];
				tabName=tyObj['TabName'];
				tabBody=tyObj['TabBody'];

                       		if(first){
                                	tabHtml += '<li class="active cwb-ty">';
                                	contentHtml += '<div class="tab-pane fade in active" id="dis-' + no + '"></div>';
                                	first=false;
                        	}else{
                                	tabHtml += '<li class="cwb-ty">';
                                	contentHtml += '<div class="tab-pane fade in" id="dis-' + no + '"></div>';
                        	}

				tabHtml+='<a href="#dis-' + no + '" data-toggle="tab" aria-label="'+gHomeTextTb['tyReport'][VER]+'">'+
				'<i class="' + obj['icon'] + ' pull-left"></i>'+ tabName +
				'<i class="fa fa-caret-down pull-right hidden-xs"></i>'+
				'</a></li>';

				o['content']= '<div class="row">' + tabBody + '</div>';
				params.push(o);
				no++;
			}
						
		}else if(warn == 'W24' && (VER == 'C')){
			
                        var o = {};

                        if(first){
                                tabHtml += '<li class="active cwb-W24">';
                                contentHtml += '<div class="tab-pane fade in active" id="dis-' + no + '"></div>';
                                first=false;
                        }else{
                                tabHtml += '<li class="cwb-W24">';
                                contentHtml += '<div class="tab-pane fade in" id="dis-' + no + '"></div>';
                        }


                        tabHtml+='<a href="#dis-' + no + '" data-toggle="tab" aria-label="'+gHomeTextTb['w24Report'][VER]+'">'+
                        '<i class="' + obj['icon'] + ' pull-left"></i>'+ WarnAllName[warn][VER] +
                        '<i class="fa fa-caret-down pull-right hidden-xs"></i>'+
                        '</a></li>';

			var w24Title = WarnAllName['W24'][VER];
			var w24Content = '';
			var w24Time = '';
			var useWID = '';
			if(WarnContent.hasOwnProperty('W24')){

				if(WarnContent['W24'][VER]['title']){

					w24Title = WarnContent['W24'][VER]['title'];
				}
			}
	
                        if(W24Type == 'TD'){

                                if(WarnContent.hasOwnProperty('W24') && WarnContent['W24'][VER]['content']){

					useWID = 'W24';
					
                                }else if(WarnContent.hasOwnProperty('W23') && WarnContent['W23'][VER]['content']){

					useWID = 'W23';

				}else if(WarnContent.hasOwnProperty('W26') && WarnContent['W26'][VER]['content']){

					useWID = 'W26';
                                }

                        }else if(W24Type == 'HR'){

                                if(WarnContent.hasOwnProperty('W24') && WarnContent['W24'][VER]['content']){

					useWID = 'W24';

                                }else if(WarnContent.hasOwnProperty('W26') && WarnContent['W26'][VER]['content']){
					
					useWID = 'W26';
                                }
                        }else{

				if(WarnContent.hasOwnProperty('W24') && WarnContent['W24'][VER]['content']){

					useWID = 'W24';
				}

			}

			if(useWID){

				var w24ContentArr = WarnContent[useWID][VER]['content'].split('\n\n');

				if(w24ContentArr.length >= 2){

					w24Time = w24ContentArr[0];
					w24Content = w24ContentArr[1];
				}
			}

                        if(w24Content){
                                var w24Html = '<div class="col-md-12">'+
                        '<h2 class="main-title">'+w24Title+'</h2>'+
                    '<div>'+
                        '<div class="content-border-gray">'+
                            '<div class="col-md-12 margin-bottom-20 margin-top-20">'+
                                '<i class="icon-color-warn warn"><i class="path1"></i><i class="path2"></i></i>'+
                                '<span class="datetime">'+w24Time+'</span>'+
                                '<p class="WarnContent">'+w24Content+'</p>'+
                            '</div>'+
                        '</div>'+
			'<a style="background-color:#c01b1b;" class="margin-10 btn btn-danger" href="/V8/'+VER+'/P/Warning/W24.html">'+gHomeTextTb['more'][VER]+'</a>'+
                    '</div>'+
                '</div>';

                        }else{

				var w24Html = '<div class="col-md-12">'+'<h2 class="main-title">'+w24Title+'</h2>'+'<a aria-label="'+gHomeTextTb['more'][VER]+'" style="background-color:#c01b1b;" class="margin-10 btn btn-danger" href="/V8/'+VER+'/P/Warning/W24.html">'+gHomeTextTb['more'][VER]+'</a>'+'</div>';

                        }
                        o['id'] = '#dis-' + no;
                        o['content'] = '<div class="row">' + w24Html + '</div>';
                        params.push(o);
                        no++;

		}
		
	}

	if(tabHtml){
		contentHtml += '</div>';

		var warnHtml='<section class="container warning-section">'+
		'<div class="row">'+
		'<div class="col-md-12">'+
		'<div class="tab-default vision_3">'+
		'<ul class="nav nav-tabs" id="tab_chart">'+
		tabHtml+
		'</ul>'+
		contentHtml+
		'</div>'+
		'</div>'+
		tyInfoHtml+
		'</div>'+
		'</section>';

		$(warnHtml).insertAfter('#header');

		for(var i=0, len=params.length; i<len; i++){
			loadWarningContent(params[i]);
		}

		// tyInfoHtml 是颱風div最下方的相關資訊如颱風動態等, 因為它獨立出來, 所以切換到別的 tab 時, 需設定event 讓它隱藏.
		if(tyInfoHtml){
			if($('.cwb-eq, .cwb-W24').hasClass('active')){
				$('.cwb-ty-info').hide();
			}

			$('.cwb-eq, .cwb-W24').click(function(){
				$('.cwb-ty-info').hide();
			});

			$('.cwb-ty').click(function(){
				$('.cwb-ty-info').show();
			});

		}

		for(var j=0, tLen=tyIds.length; j<tLen; j++){
			$('#dis-'+tyIds[j]).find('h3').append('<a aria-label="'+gHomeTextTb['more'][VER]+'" style="background-color:#c01b1b;" class="margin-left-10 btn btn-danger" href="/V8/'+VER+'/P/Typhoon/TY_WARN.html">'+gHomeTextTb['more'][VER]+'</a>');

			$('#dis-'+tyIds[j]).find('.WPPS_DIV').append(WPPS_BTN[VER]);

			//豪雨特報按鈕.
			if(WarnFIFOWS.indexOf('W26') > -1){
				var W26_title = '';
				if(typeof WarnContent.W26 !== 'undefined'){
					W26_title = WarnContent['W26'][VER].title;
				}
				var btnDivClass = {'C':'col-sm-6', 'E':'col-sm-12'};
				var W26_BTN = ''+
            '<div class="'+btnDivClass[VER]+'">'+
                '<a class="btn btn-default btn-block margin-top-10" title="'+gHomeTextTb['click2check'][VER]+'" target="_self" href="/V8/'+VER+'/P/Warning/W26.html?T='+File_Time+'">'+W26_title+'</a>'+
            '</div>';	
				$('#dis-'+tyIds[j]).find('.WPPS_DIV').append(W26_BTN);
			}

			
		}

	}
}

//廣告
function loadHomeNews(){

	var src='/V8/'+VER+'/ajax/_home_news.html';
	var selector='.jumbotron_carousel';
	var gMouseOver = false;

	//無障礙.
	$('head').append('<style>.AD-item>a:focus{border: none !important;} .AD-item>a:focus>img{border: #FFBF47 solid 0.5em !important;}</style>');

	$.ajax({url:src,cache:false}).done(function(data){
		
		$(selector).html(data).owlCarousel({
			navText: ['<button style="background:none;border:none;" type="button" role="presentation" aria-label="'+gHomeTextTb['prevPage'][VER]+'"><i class="fa fa-angle-left" aria-hidden="true"></i></button>', '<button style="background:none;border:none;" type="button" role="presentation" aria-label="'+gHomeTextTb['nextPage'][VER]+'"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'],
			nav: true,
			loop: false,
			rewind: true,
			dots: true,
			responsive:{
				100:{
					items:1
				},
			},
			autoplay:true,
			autoplayTimeout:8000, // time for slides changes
			smartSpeed: 1000,     // duration of change of 1 slide
			autoplayHoverPause:true
		});

		//以下都是無障礙

                var controlPlay = function(mode){
                        var $selector = $(selector);
                        var c = $selector.data('owl.carousel');

                        if(mode == 'play' && !c.settings.autoplay){

                                $selector.trigger('play.owl.autoplay');
                                c.settings.autoplay = true;
                                c.options.autoplay = true;
                                $selector.trigger('refresh.owl.carousel');

                        }else if(mode == 'stop' && c.settings.autoplay){

                                $selector.trigger('stop.owl.autoplay');
                                c.settings.autoplay = false;
                                c.options.autoplay = false;
                                $selector.trigger('refresh.owl.carousel');
                        }
                };

                $(document).keydown(function(e){
                        if(e.which == 9){
                                gMouseOver = false;
                        }
                });

		// owl 很多 bug. 文件也不清楚. 如果看他們的文件來使用API 很多都不能work. 
		$('.AD-item > a').on('focus', function(e){
			
			var $me = $(this);
			var index = $me.parents('.owl-item').index();
			controlPlay('stop');
			if(!gMouseOver){
				// 一定要 translate3d(0px ,0px, 0px) 才能 tab 到每一張圖.
				$me.closest('.owl-stage').css('transform', 'translate3d(0px ,0px, 0px)');
			}

			//如果設定 $selector.trigger('to.owl.carousel', [index, 0]); 也沒用, 會有無法顯示特定圖片的問題. 這是 bug.
			//第一個必須要設0, 才能tab到每一張圖, 似乎是 owl的bug. 也可能是 Chrome的 bug. 但是這樣滑鼠click 會跑到第一張.
			//$selector.trigger('to.owl.carousel', [0, 0]); 
						
			$(selector + ' .owl-dot').removeClass('active').eq(index).addClass('active');
			
		}).mouseover(function(){
			gMouseOver = true;
			controlPlay('stop');

		}).mouseout(function(){
			gMouseOver = false;
			controlPlay('play');
		});
	
		$('.popup-text-cwb').click(function(){
			var text = $(this).data('text');
			var html = '<div class="modal fade cwb-ad-text" id="marquee_1" tabindex="-1" role="dialog" aria-labelledby="marqueeLabel">' +
                                '<div class="modal-dialog" role="document">' +
                                '<div class="modal-content">' +
                                '<div class="modal-body" id="marqueeLabel">' +
                                '<p>'+ text + '</p>' +
                                '</div>' +
                                '<div class="modal-footer">' +
                                '<button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="'+gHomeTextTb['close'][VER]+'">'+gHomeTextTb['close'][VER]+'</button>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
			
			$('.cwb-ad-text').remove();
			$('#footer-v3').after(html);

		});
		
	}).fail(function(xhr, status, error){
		console.log('Loading ' + src + ' failed: ' + error);
	});	
}

function showNews(){
	//因為有可能中文版有公告, 英文版沒有, 故需分開.
	//為了避免永遠看不到新公告之問題, 每次更改公告, 需更改 ts[VER]的值, 建議設定為當下日期. 若是設公告內容為空字串(''), 可不必修改 ts[VER].
	//注意, 也須更改首頁內的本js?v=後面的時間, 才能確保使用者看到.
	var ts = {'C': '2019-10-30', 'E': '2019-10-30'};
	//若不想公告就設為空字串(即: '' ).
	var newsStr = {'C':'氣象局公告：<br>您好，本局已推出切換電腦版、手機版模式的功能，用戶可利用此功能選擇喜歡的瀏覽模式，請滑到最下方選取您所想檢視的版本，謝謝。<br>註：使用手機裝置瀏覽本局網頁之用戶才會有此功能，若您使用電腦裝置瀏覽本局網頁，則不會看到切換模式之選項。',
	'E':'News:<br>Mobile device users can select desktop or mobile version page on the bottom.'};

        if((newsStr[VER] != '') && (getCookie('V8_' + VER + '_see_news') != ts[VER])){

		var forMobile = true; //若為 true, 則限定為只有手機可看, 若為 false, 則手機電腦皆可看.
                if(forMobile && !isMobile_()){
                        return;
                }

                var html = '<div id="CWB-news-module">'+
    '<div class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="CWB-marqueeLabel" style="display: block;">'+
        '<div class="modal-dialog" role="document">'+
            '<div class="modal-content" id="CWB-marqueeLabel">'+
                '<div class="modal-body" style="padding: 40px;">' + newsStr[VER] + '</div>'+
                '<div class="modal-footer">'+
                    '<button type="button" id="news-off-btn" class="btn btn-primary" data-dismiss="modal" aria-label="'+gHomeTextTb['close'][VER]+'">'+gHomeTextTb['close'][VER]+'</button>'+
                '</div>'+
            '</div>'+
        '</div>'+
    '</div>'+
    '<div class="modal-backdrop fade in"></div>'+
'<div>';
                $("#footer-v3").append(html);
                $('#news-off-btn').click(function(){
                        setCookie('V8_' + VER + '_see_news', ts[VER]);
                        $('#CWB-news-module').remove();
                });
        }
}

$(document).ready(function(){	
	//console.log('V8_Home_read_data is called');
	//必須要按照順序, 分別是tab-1, tab-2, tab-3, tab-4, tab-5 注意其中Notice格式與NewsHot, Newsbb格式不同
	//英文版只有: 最新消息(Event), 新聞稿(News), 熱門話題(Hot Topic)
	var items={'C':[{'title':'最新消息連結','data':NewsHot[VER]}, {'title':'新聞稿連結','data':Newsbb[VER]}, 
		{'title':'政府資訊公開連結','data':Notice}, {'title':'討論話題','data':HotTopic},
		{'title':'熱門話題','data':HotTalk}, {'title':'前瞻計畫專區', 'data':ProspectivePlan}],
		'E':[{'title':'Event','data':NewsHot},{'title':'News','data':Newsbb},{'title':'HotTopic','data':HotTalk}]};

	
	//HomeHeaderNews.js 是一定要載入的檔案.
	var wLen = WarnHome.length; 
	//預設 callback.
	//判斷重要公告div或警特報div是否存在, 若存在就移動導盲磚
	//'重要公告'是一塊模組, 很多地方都會用到, 若用 class 名稱可能會誤判, 所以加上名稱.

	var cb1 = function(){
        	if($('.warning-section').length > 0 || $('#important-notice').length > 0){

                	$('#header').after($('#center-content-div'));
        	}
	};

	var callback = function(){
		// 順序非常重要, 不可任意更改
		// 因為它們都是將資料載入到 #header 的後面, 其中 HomeHeaderNews(重要公告)應該要永遠在最上方. 故這裡順序很重要.
		loadWarning();
		loadHomeHeaderNews();
		cb1();
	};
	//如果在 loadWarning() 函數內判斷並載入需要的檔案, 有點困難..., 所以先在這邊簡單判斷, 載入需要的檔案, 然後呼叫 loadWarning()
	
	var tyUrl = '/Data/js/typhoon/TY_WARN-Data.js';
	var w24Url = '/Data/js/warn/W24-Data.js';
	var homeHeaderNewsUrl = '/Data/js/HomeHeaderNews.js';

	if(wLen == 0){

		callback = function(){
			loadHomeHeaderNews();
			cb1();
		}

                $.when($.ajax({url: homeHeaderNewsUrl, dataType: 'script', cache: false}))
                        .then(callback, function(){console.log('load js files fail!');}
                );

	}else if(wLen == 1){

		if(WarnHome[0] == 'EQ'){

			$.when($.ajax({url: homeHeaderNewsUrl, dataType: 'script', cache: false}))
			.then(callback, function(){console.log('load js files fail!');});

		}else if(WarnHome[0] == 'TY_WARN'){

			$.when($.ajax({url: tyUrl, dataType: 'script', cache: false}),
			$.ajax({url: homeHeaderNewsUrl, dataType: 'script', cache: false}))
			.then(callback, function(){console.log('load js files fail!');});

		}else if(WarnHome[0] == 'W24'){

			$.when($.ajax({url: w24Url, dataType: 'script', cache: false}),
			$.ajax({url: homeHeaderNewsUrl, dataType: 'script', cache: false}))
			.then(callback, function(){console.log('load js files fail!');});

		}

	}else if(wLen > 1){ 
			//要一個一個判斷有點麻煩, 共有C(M,N)組合, 且未來有可能增加, 不會是只有目前的C(3,2)=3種,
			//考慮到同時有兩個以上的情況不多, 用這種寫法應不會浪費太多流量.
		
                $.when($.ajax({url: tyUrl, dataType: 'script', cache: false}),
                        $.ajax({url: w24Url, dataType: 'script', cache: false}),
                        $.ajax({url: homeHeaderNewsUrl, dataType: 'script', cache: false}))
                        .then(callback, function(){console.log('load js files fail!');}
                );
	}
	loadHomeNews();
	loadHotNews(items[VER]);
	loadGoodSites();
	showNews();
	//這裡使用 lazy load 技術
	//最新消息若使用這種lazy load會讓 tab 怪怪的, 好站介紹會無法 tab到, 故目前讓它們一開始就load.
	//另外, 廣告用這種lazy load效益不大, 因為在多數情況它一開始就是visible, 所以也不用lazy load.
	//目前英文版尚未有需用到 load lazy 的 dom.
	var objsToLoad = {'C': {'.video-main':loadVideoList}, 'E':{}};
	var count= Object.keys(objsToLoad[VER]).length;
	if(count > 0 ){
		$(window).scroll(function(){
			var $w = $(window);
			var wt = $w.scrollTop();
			var wb = wt + $w.height();
			for(var name in objsToLoad[VER]){	
				var $obj = $(name);
				var obLoaded = $obj.hasClass('cwb-loaded');
				if(!obLoaded){
					var ot = $obj.offset().top;
					var ob = ot + $obj.height();
					if(wt <= ob && wb >= ot){
						objsToLoad[VER][name]();
						$obj.addClass('cwb-loaded');
						count--;
					}
				}
			}
			if(count==0){
				$w.off('scroll');
			}
		}).scroll();	
	}
});
