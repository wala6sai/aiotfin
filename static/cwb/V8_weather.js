//Author: Bocheng
//Date: 2018/06/01
//修改 assets/js/observe/function-index.js
//用於 V8 首頁.

var gCID; //可能是 CXXXX (前面有一個'C'字母) or 純數字 XXXXX
var gInterval;
var gTargetNum;
var gTargetSec;
var gFavorite = [];
var gAirQuality;
var gHourIssued;
var FAVORITE_MAX_LEN=4; //我的最愛上限4個
var FAVORITE_KEYWORD="V8_FAVORITE"; // 我的最愛存cookie的字首.
var WEATHER_SVG_ICON_DIR="/V8/assets/img/weather_icons/weathers/svg_icon/";

//地圖位置
var gPosition = {
  "C10017": "310 30",
  "C65": "280 65",
  "C63": "288 30",
  "C68": "240 55",
  "C10004": "240 100",
  "C10018": "210 80",
  "C10005": "210 130",
  "C66": "180 170",
  "C10008": "210 220",
  "C10007": "150 210",
  "C10009": "130 235",
  "C10010": "180 275",
  "C10020": "142 265",
  "C67": "130 320",
  "C64": "130 380",
  "C10013": "170 420",
  "C10014": "225 350",
  "C10015": "275 220",
  "C10002": "300 115",
  "C09007": "20 80",
  "C09020": "20 150",
  "C10016": "15 250"
};

//手機版用的
var gForm='<form action="" class="sky-form">'+
'<label class="select">'+
'<select name="gender">';
/*
for(var i=0,len=Info_County.length; i<len; i++){
	gForm += '<option value="C' + Info_County[i]['ID'] + '">' + Info_County[i]['Name'][VER] + '</option>';
}*/
/*
'<option value="C10017">基隆市</option>'+
'<option value="C63">臺北市</option>'+
'<option value="C65">新北市</option>'+
'<option value="C68">桃園市</option>'+
'<option value="C10018">新竹市</option>'+
'<option value="C10004">新竹縣</option>'+
'<option value="C10005">苗栗縣</option>'+
'<option value="C66">臺中市</option>'+
'<option value="C10007">彰化縣</option>'+
'<option value="C10008">南投縣</option>'+
'<option value="C10009">雲林縣</option>'+
'<option value="C10020">嘉義市</option>'+
'<option value="C10010">嘉義縣</option>'+
'<option value="C67">臺南市</option>'+
'<option value="C64">高雄市</option>'+
'<option value="C10013">屏東縣</option>'+
'<option value="C10014">臺東縣</option>'+
'<option value="C10015">花蓮縣</option>'+
'<option value="C10002">宜蘭縣</option>'+
'<option value="C10016">澎湖縣</option>'+
'<option value="C09020">金門縣</option>'+
'<option value="C09007">連江縣</option>'+
*/

/*
gForm+='</select>'+
'<i></i>'+
'</label>'+
'</form>';*/

/*說明:
TD: 今日白天 => day
TN: 今晚明晨 => night
TM: 明日白天 => day
TMN: 明日晚上 => night
*/
var gIconTypeTable={'TD':'day', 'TN':'night', 'TM':'day', 'TMN':'night'};
var gTimeDesc={
	'TD':{'C':'今日白天','E':'Today'},
	'TN':{'C':'今晚明晨','E':'Tonight'},
	'TM':{'C':'明日白天','E':'Tomorrow'},
	'TMN':{'C':'明日晚上','E':'Tomorrow Night'},
	'-':{'C':'-','E':'-'}
};
var gTimeDescShift={
	'TD':{'C':'昨日白天','E':'Yesterday'},
	'TN':{'C':'昨晚今晨','E':'Last night'},
	'TM':{'C':'今日白天','E':'Today'},
	'TMN':{'C':'今日晚上','E':'Tonight'},
	'-':{'C':'-','E':'-'}
};

//讀取json
$(function() {
	for(var i=0,len=Info_County.length; i<len; i++){
        	gForm += '<option value="C' + Info_County[i]['ID'] + '">' + Info_County[i]['Name'][VER] + '</option>';
	}
	gForm+='</select>'+
	'<i></i>'+
	'</label>'+
	'</form>';

	var mmddIssued = IssuedTime_36hr.substr(0,5);
	gHourIssued = parseInt(IssuedTime_36hr.substr(6,2));
	$(".datetime li:eq(0)").html('<span class="date">' + mmddIssued + '</span>');

	initPage();
	
});
// 給定縣市代碼, 取得縣市名稱, by Bocheng.
function getCountyName(id, ver){
	var len=Info_County.length;
	for(var i=0; i<len; i++){
		if(Info_County[i]['ID']==id){	
			return Info_County[i]['Name'][ver];
		}
	}
}
/*
function getInitTime(time){
	time=time.replace('/','-');
	time=time.replace(/\s+/,'T');
	var d=new Date();
	var y=d.getFullYear();
	time=y+'-'+time;
	return time;
}*/

//by Bocheng.
//2018/06/04
function loadFavorite(){
	//deleteCookie(FAVORITE_KEYWORD);
	//XXX: 需考慮到 cookie出現異常時, 有無辦法偵測並排除? 例如, 明明畫面只顯示3個縣市, 應該是可以再增加一個縣市到我的最愛,
	//但有時候cookie出現異常, 就沒辦法繼續增加了!
	// 我有時會這樣, 不知道是程式問題還是怎樣?
	var value;
	value=getCookie(FAVORITE_KEYWORD);
	//console.log(value);
	if(value){
		var arr=value.split("-");
		for(var i=0; i<arr.length; i++){
			addFavorite(arr[i]);
		}
		gCID=arr[0]; //第一個我的最愛(有愛心標示的)
	}else{
		gCID="C63" //若無設定, 預設臺北.
	}
}

function updateFavoriteCookie(arr){
	//注意, arr可能是空array, join返回的是 "" 空字串.
	//XXX:這裡是否要再檢查array是否為空??	
	var str=arr.join("-");
	setCookie(FAVORITE_KEYWORD, str);
}

function addFavorite(townshipID) {

  var wordTb = {'tip1': {'C': '已加入到我的最愛', 'E': 'It has been added to your favorites'},
		'tip2': {'C': '我的最愛不得超過4個', 'E': 'The maximum number of favorites is 4'}
  };
  if (gFavorite.length < FAVORITE_MAX_LEN) {
    var favoriteFlag = 0;
    for (var i = 0; i <= gFavorite.length; i++) {
      if (gFavorite[i] == townshipID) {
        favoriteFlag = 1; //代表已加到我的最愛. 
      }
    }
    if (favoriteFlag != 1) { 
      gFavorite.push(townshipID);
      updateFavorite();
    } else {
      $('#alert').html('<div class="alert-block"><div class="alert-inner"><i class="fa fa-info-circle"></i> <span>'+wordTb['tip1'][VER]+'</span></div></div>');
      setTimeout(function(){ $('#alert').html(''); }, 3100);
    }
  } else {
    $('#alert').html('<div class="alert-block"><div class="alert-inner"><i class="fa fa-info-circle"></i> <span>'+wordTb['tip2'][VER]+'</span></div></div>');
    setTimeout(function(){ $('#alert').html(''); }, 3100);
  }
}

function option(index) {
  $('ul.favorite > li:eq(' + index + ') > span:eq(0)').toggleClass("ovr_on");
}

function removeFavorite(index) {

  gFavorite.splice(index, 1);
  updateFavorite();
}

function setMain(index) {
  var favoriteData = gFavorite[index];
  gFavorite.splice(index, 1);
  gFavorite.unshift(favoriteData);
  updateFavorite();
}

function updateFavorite(){

	var wordTb={'enter':{'C':'進入','E':'Enter '},'forecast':{'C':'詳細預報', 'E':' forecast'}, 'primary':{'C':'設置主位置','E':'Set as primary position'},
		'remove':{'C':'移除','E':'Remove'},'remove2':{'C':'移除我的最愛','E':'Remove my favorite'}};
	updateFavoriteCookie(gFavorite);
	var favoriteHTML='';
	for(var i=0; i<gFavorite.length; i++){
		var id=gFavorite[i];
		var countyName=getCountyName(id, VER);
		favoriteHTML += '<li><span class="tag"><a title="' + wordTb['enter'][VER] + countyName + wordTb['forecast'][VER] + '" href="/V8/'+VER+'/W/County/County.html?CID=' + id + '" class="city-name">' + countyName + '</a><a role="button" onclick="option(' + i + ')"><i class="' + ((i == 0) ? "icon-heart" : "fa fa-ellipsis-v") + '"></i></a></span>' +
        	'<span class="log">' +
        	'<ul>';

		if(i!=0){
			favoriteHTML += '<li><a title="'+wordTb['primary'][VER]+'" role="button" onclick="setMain(' + i + ')"><i class="icon-heart" aria-hidden="true"></i>'+wordTb['primary'][VER]+'</a></li>';
		}

		favoriteHTML += '<li><a title="'+wordTb['remove2'][VER]+'" role="button" onclick="removeFavorite(' + i + ')"><i class="fa fa-times"></i>'+wordTb['remove'][VER]+'</a></li>' +
		'</ul>' +
        	'</span></li>';
	}
	$('ul.favorite').html(favoriteHTML);

	//當使用者加入我的最愛, 會自動選擇縣市&鄉鎮預報的縣市.
	var mainCity = gFavorite[0];
	if(!mainCity){
		mainCity = '';
	}
	var megaCity = $('#megacity').find('section').eq(0);
	var megaTown = $('#megatown').find('section').eq(0);
	megaCity.find('option[value="' + mainCity +'"]').prop('selected', true);
	megaTown.find('option[value="' + mainCity + '"]').prop('selected', true);
	megaTown.find('select').change();
}

var gTownOptionName = {'C':'選擇鄉鎮', 'E':'Choose a Township'};
function buildTown(){
	var id = getID();
	var $selectTown = $('#home-select-town');
	$selectTown.html('<option value="" disabled="" selected="">' + gTownOptionName[VER] + '</option>');
	$.each(Info_Town[id], function(i){
		$selectTown.append('<option value="' + Info_Town[id][i].ID + '">' + Info_Town[id][i].Name[VER] + '</option>');
	});
}

function initPage() {
//	console.log('V8_weather.js called'); 
  $('#default_map > svg > a').on('click focus touchend', function() {
    $('#default_map > svg > a').attr("class", "");
    $(this).attr("class", "is-active");
    gCID = $(this).find("g").attr("id");
    updatePosition();
    updateWeather();
  }).on('mouseover focus', function(){

    var id = $(this).find("g").attr("id");
    $('#default_map > .weather_data > a').removeClass('on-hover');
    $('#default_map > .weather_data > a[name="' + id + '"]').addClass('on-hover').css('border', '2px solid orange');

  }).on('mouseout blur', function(){

    $('#default_map > .weather_data > a').removeClass('on-hover');

  });
  // for accessibility. Bocheng.
  $('#default_map > .weather_data > a').focus(function(){
	$('#default_map > svg > a:eq(0)').focus();	        
  });
  $('#default_map > svg > a:eq(0)').keydown(function(e){

	if(e.shiftKey && e.which == 9){
		$('.jumbotron_carousel .owl-item:last .AD-item a').focus(); // V8_Home_read_data.js
	}
  });

  $('#default_map > svg >a').keydown(function(e){

	// 13 = Enter
	if(e.which == 13){
		gCID = $(this).find("g").attr("id");
		window.location.href = '/V8/' + VER + '/W/County/County.html?CID=' + getID() ;
	}
  });

  $('#home-form').submit(function(){

	var url;
	var val = $(this).find('select').val();

	if(val === null){
		url = '/V8/' + VER + '/W/Town/Map_' + getID() + '.html';
	}else{
		url = '/V8/' + VER + '/W/Town/Town.html?TID=' + val;
	}

	window.location.href = url;
	return false;
  });

  var callback=function(){

        $('#checkbox-1').click(function(){
                updateWeather();
        });

        $('#checkbox-1_sm').click(function(){
                updateWeather();
        });

        $('#checkbox-2').click(function(){
                updateWeather();
        });

        $('#checkbox-2_sm').click(function(){
                updateWeather();
        });
  };

  addCallbackForHeader(callback);

  $('.weather-loc').on('change', 'select[name=gender]', function(){

	var id=$(this).val();
	gCID=id;
	// START 下兩行不知道需要嗎? PC版才看得到地圖的...
	updatePosition();
	setMapHighlight();	
	// END
	updateWeather();
  });

  loadFavorite();
  updatePosition();
  setMapHighlight();
  updateWeather();
}

//取得前面有 'C' 的ID, 例如: CXXXXXX.
function getCID(){

	if(gCID.substr(0,1)!='C'){
		return 'C'+gCID;
	}else{
		return gCID;
	}

}
//取得 純數字 XXXXXX.
function getID(){
	if(gCID.substr(0,1)=='C'){
		return gCID.substr(1);
	}else{
		return gCID;
	}
}
// 更新地圖上的白區塊 (預設是在臺北)
// 這是因為希望地圖將地圖反白到該縣市.
function setMapHighlight(){
	var id=getCID();
	$('#default_map svg a').attr("class", "");
	var ele=$('#default_map svg a g#'+id);
	ele.parent().attr('class', 'is-active');	
}
//這裡是設定臺灣地圖上的游標.
function updatePosition() {
	var id=getCID();
  	$('#position').attr("transform", "translate(" + gPosition[id] + ")");
}

function updateWeather(){

	var id;
	id=getID();
	var countyName=getCountyName(id, VER);
	var data = TableData_36hr[id];
	var iconType;
	var wordTb = {'more': {'C':'看更多', 'E':'More'}, 
	'addFavorite':{'C':'加入我的最愛', 'E':'Add to my favorites'},
	'station':{'C':'測站名稱','E':'Station'},
        'dataSource':{'C':'資料來源','E':'Source'},
        'epa':{'C':'環保署','E':'EPA'},
	'sunRiseSet':{'C':'日出日落示意圖','E':'sunrise-sunset'}};
	// 一定要將 id 加上單引號, 不然 純數字的字串會誤判成數字, 例如: addFavorite( 09020 ) , 會認為是 addFavorite( 9020 ), 而產生錯誤.

	var f = function(){	
		$('.weather-loc div[class="city"]').html( gForm + '<span><a href="/V8/'+VER+'/W/County/County.html?CID='+id+'" aria-label="'+wordTb['more'][VER]+'">' + countyName + '</a></span><a onclick="addFavorite(' + "'" + id + "'" +')" role="button" href="javascript:void(0)" townshipid=' + id + ' aria-label="'+wordTb['addFavorite'][VER]+'"><i class="icon-heart"></i></a>' + '<a href="/V8/'+VER+'/W/County/County.html?CID='+ id + '" aria-label="'+wordTb['more'][VER]+'" class="index-seeMore"><i class="fa fa-plus-square" aria-hidden="true"></i>'+wordTb['more'][VER]+'</a>');
	
		var cid=getCID();
		$('select[name=gender] option[value="'+cid+'"]').attr("selected",'selected');
		buildTown();
	}
	//強迫手機版選單重新繪圖. Chrome 不會重新繪圖, 若點選某縣市按下確定, 再按上一頁, 會停留上一個選擇的縣市而造成誤會.
	setTimeout(f);

	var tempUnit=getTempUnit();
	var nowHour = new Date().getHours();
	var timeDesc='';
	//一定只有3筆, 也只能有3筆.
	for(var i=0; i<3; i++){
		var obj=data[i];
		iconType=gIconTypeTable[obj['Type']];

		if( (gHourIssued >= 17)  && (nowHour >= 0) && (nowHour < 11) ){
			timeDesc=gTimeDescShift[obj['Type']][VER];	
		}else{
			timeDesc=gTimeDesc[obj['Type']][VER];	
		}

		//第1筆跟第2,3筆的html不一樣, 分開
		if(i==0){
			var tRange=obj['TimeRange'];
			var tArr=tRange.split('~');
			var t1=tArr[0].trim().split('-')[1];
			var t2=tArr[1].trim().split('-')[1];
			$('.today-tem .moment').html(timeDesc);
			$(".datetime li:eq(1)").html(t1 + "~" + t2);
			$('.weather-loc span[class="rain"]').html("<i class=\"icon-umbrella\"></i>" + obj['PoP'] + "%");
			var imgStr = '<img src="'+WEATHER_SVG_ICON_DIR + iconType + "/" + obj['Wx_Icon'] + ".svg" + '" alt="'+obj['Wx']+'" title="'+obj['Wx']+'">';
			$('.weather-loc span[class="icon"]').html(imgStr);
			$('.weather-loc i[class="low"]').html(obj['Temp'][tempUnit]['L'] + "<sup>°</sup>");
			$('.weather-loc i[class="height"]').html(obj['Temp'][tempUnit]['H'] + "<sup>°</sup>");
			
		}else{
			var divNum=i-1;
			$('.weather-days div:eq('+divNum+') span[class="week-day"]').html(timeDesc);
			var imgStr = '<img src="'+WEATHER_SVG_ICON_DIR + iconType + "/" + obj['Wx_Icon'] + ".svg" + '" alt="'+obj['Wx']+'" title="'+obj['Wx']+'">';
			$('.weather-days div:eq('+divNum+') span[class="icon"]').html(imgStr);
			$('.weather-days div:eq('+divNum+') span[class="tem"]').html("<i class=\"low\">" + obj['Temp'][tempUnit]['L'] + "<sup>°</sup></i><i class=\"height\">" + obj['Temp'][tempUnit]['H'] + "<sup>°</sup></i>");
			$('.weather-days div:eq('+divNum+') span[class="rain"]').html("<i class=\"icon-umbrella\"></i>" + obj['PoP'] + "%");	

		}

	}

	//空氣品質
	if(AirQuality === null){

		$('.airbar').hide();
		$('#air-sitename').html(wordTb['more'][VER]);
		$('#air-datatime').html(wordTb['dataSource'][VER]+'：'+wordTb['epa'][VER]);

	}else{
      		gAirQuality = parseInt(AirQuality[id]['AQI']);
      		//參考環保署網站定義的標準: https://taqm.epa.gov.tw/taqm/tw/default.asp
      		if (gAirQuality >= 0 && gAirQuality <= 50) {
        		$('.airbar').attr("id", "level-1");
      		} else if (gAirQuality >= 51 && gAirQuality <= 100) {
        		$('.airbar').attr("id", "level-2");
      		} else if (gAirQuality >= 101 && gAirQuality <= 150) {
        		$('.airbar').attr("id", "level-3");
      		} else if (gAirQuality >= 151 && gAirQuality <= 200) {
        		$('.airbar').attr("id", "level-4");
      		} else if (gAirQuality >= 201 && gAirQuality <= 300) {
        		$('.airbar').attr("id", "level-5");
      		} else if (gAirQuality >= 301 && gAirQuality <= 500) {
        		$('.airbar').attr("id", "level-6");
      		}

      		if (gAirQuality >= 0 && gAirQuality <= 100) {
        		$('.airicon i').attr("class", "icon-warn_PM_low");
      		} else if (gAirQuality >= 101) { // 與生活氣象app一樣的標準.
        		$('.airicon i').attr("class", "icon-warn_PM");
     		}

      		$('#air-sitename').html(wordTb['station'][VER]+'：'+AirQuality[id]['SiteName'][VER]);
      		var pt=AirQuality[id]['PublishTime'];
      		//$('#air-datatime').html('空氣品質資料時間：'+pt.substr(5));
      		$('#air-datatime').html(wordTb['dataSource'][VER]+'：'+wordTb['epa'][VER]+' ('+pt.substr(5)+')');
      		gInterval = null;
      		gTargetNum = 0;
      		gTargetSec = 2000 / gAirQuality;
      		updateAirBar();
	}
}

function updateAirBar() {
  gInterval = setInterval(function() {
    if (gTargetNum <= gAirQuality) {
      $('.level span:eq(1)').text(gTargetNum);
      gTargetNum++;
    } else {
      clearInterval(gInterval);
      gInterval = null;
    }
  },  gTargetSec);
}

function getVersion() { //取得 minNum(最小值) ~ maxNum(最大值) 之間的亂數
  return Math.floor(Math.random() * (1000 - 1 + 1)) + 1;
}
	
