"""aiotfin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from mainsite.views import index, charts, tables, info_slide
from mainsite.views import map_city, error_404
from mainsite.views import test, index_re#, goods_in, goods_out #, monthcount, layout_sidenav_light, cwb
# from mainsite.views import goods_in, goods_out, goods_hot, goods_stable, goods_few, goods_lack

urlpatterns = [
    path('test/', test),
    path('', index),
    path('admin/', admin.site.urls),
    path('index/', index),
    path('index/*', index),
    path('index_re/', index_re),
    path('map/', map_city),
    path('charts/', charts),
    path('tables/', tables),
    path('info_slide/', info_slide),
    path('404/', error_404),
    # path('cwb/', cwb),
    # path('tables/goods_in/', goods_in),
    # path('tables/goods_out/', goods_out),
    # path('tables/monthcount/', monthcount),
    # path('tables/goods_hot/', goods_hot),
    # path('tables/goods_stable/', goods_stable),
    # path('tables/goods_few/', goods_few),
    # path('tables/goods_lack/', goods_lack),
    # path('map/<int:city_no>', map_city, name = 'city-url'),
    # path('layout-sidenav-light/', layout_sidenav_light),
]
