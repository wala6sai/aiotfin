from django.shortcuts import render
from django.urls import reverse

def test(request, city_no=0, few_no = 0, lack_no=0):
    city_list=[
        {"cityname":"宜蘭縣","citycode":"C10002"},
        {"cityname":"新竹縣","citycode":"C10004"},
        {"cityname":"苗栗縣","citycode":"C10005"},
        {"cityname":"彰化縣","citycode":"C10007"},
        {"cityname":"南投縣","citycode":"C10008"},
        {"cityname":"雲林縣","citycode":"C10009"},
        {"cityname":"嘉義縣","citycode":"C10010"},
        {"cityname":"屏東縣","citycode":"C10013"},
        {"cityname":"臺東縣","citycode":"C10014"},
        {"cityname":"花蓮縣","citycode":"C10015"},
        {"cityname":"澎湖縣","citycode":"C10016"},
        {"cityname":"基隆市","citycode":"C10017"},
        {"cityname":"新竹市","citycode":"C10018"},
        {"cityname":"嘉義市","citycode":"C10020"},
        {"cityname":"臺北市","citycode":"C63"},
        {"cityname":"高雄市","citycode":"C64"},
        {"cityname":"新北市","citycode":"C65"},
        {"cityname":"臺中市","citycode":"C66"},
        {"cityname":"臺南市","citycode":"C67"},
        {"cityname":"桃園市","citycode":"C68"},
        {"cityname":"連江縣","citycode":"C09007"},
        {"cityname":"金門縣","citycode":"C09020"},
    ]
    city = city_list[city_no]
    few_list=[
        {"name":"手拿包","num_now":"12","num_safe":"10"},
        {"name":"郵差包","num_now":"10","num_safe":"10"},
        {"name":"背包","num_now":"18","num_safe":"15"},
        {"name":"錢包","num_now":"8","num_safe":"5"},
        {"name":"托特包","num_now":"30","num_safe":"25"},
        {"name":"水桶包","num_now":"15","num_safe":"15"},
        {"name":"手提包","num_now":"35","num_safe":"30"},
    ]
    fewones = few_list[few_no]
    lack_list=[
        {"name":"手拿包","num_now":"6","num_safe":"10"},
        {"name":"郵差包","num_now":"4","num_safe":"10"},
        {"name":"背包","num_now":"10","num_safe":"15"},
        {"name":"錢包","num_now":"2","num_safe":"5"},
        {"name":"托特包","num_now":"20","num_safe":"25"},
        {"name":"水桶包","num_now":"10","num_safe":"15"},
        {"name":"手提包","num_now":"15","num_safe":"30"},
    ]
    lackones = lack_list[lack_no]
    return render(request, 'test.html', locals())
def index_re(request):
    return render(request, 'index_re.html')
def layout_sidenav_light(request):
    return render(request, 'layout-sidenav-light.html')
def cwb(request):
    return render(request, 'cwb.html')



# Create your views here.

def index(request, city_no=0, few_no = 0, lack_no=0):
    city_list=[
        {"cityname":"宜蘭縣","citycode":"C10002"},
        {"cityname":"新竹縣","citycode":"C10004"},
        {"cityname":"苗栗縣","citycode":"C10005"},
        {"cityname":"彰化縣","citycode":"C10007"},
        {"cityname":"南投縣","citycode":"C10008"},
        {"cityname":"雲林縣","citycode":"C10009"},
        {"cityname":"嘉義縣","citycode":"C10010"},
        {"cityname":"屏東縣","citycode":"C10013"},
        {"cityname":"臺東縣","citycode":"C10014"},
        {"cityname":"花蓮縣","citycode":"C10015"},
        {"cityname":"澎湖縣","citycode":"C10016"},
        {"cityname":"基隆市","citycode":"C10017"},
        {"cityname":"新竹市","citycode":"C10018"},
        {"cityname":"嘉義市","citycode":"C10020"},
        {"cityname":"臺北市","citycode":"C63"},
        {"cityname":"高雄市","citycode":"C64"},
        {"cityname":"新北市","citycode":"C65"},
        {"cityname":"臺中市","citycode":"C66"},
        {"cityname":"臺南市","citycode":"C67"},
        {"cityname":"桃園市","citycode":"C68"},
        {"cityname":"連江縣","citycode":"C09007"},
        {"cityname":"金門縣","citycode":"C09020"},
    ]
    city = city_list[city_no]
    few_list=[
        {"name":"手拿包","num_now":"12","num_safe":"10"},
        {"name":"郵差包","num_now":"10","num_safe":"10"},
        {"name":"背包","num_now":"18","num_safe":"15"},
        {"name":"錢包","num_now":"8","num_safe":"5"},
        {"name":"托特包","num_now":"30","num_safe":"25"},
        {"name":"水桶包","num_now":"15","num_safe":"15"},
        {"name":"手提包","num_now":"35","num_safe":"30"},
    ]
    fewones = few_list[few_no]
    lack_list=[
        {"name":"手拿包","num_now":"6","num_safe":"10"},
        {"name":"郵差包","num_now":"4","num_safe":"10"},
        {"name":"背包","num_now":"10","num_safe":"15"},
        {"name":"錢包","num_now":"2","num_safe":"5"},
        {"name":"托特包","num_now":"20","num_safe":"25"},
        {"name":"水桶包","num_now":"10","num_safe":"15"},
        {"name":"手提包","num_now":"15","num_safe":"30"},
    ]
    lackones = lack_list[lack_no]
    earn_list=[
        {"year":"109","month":"01","earnM":"100000","cost":"68000","profit":"32000","Gmargin":"42.9%"},
        {"year":"109","month":"02","earnM":"80000","cost":"57000","profit":"23000","Gmargin":"40.4%"},
        {"year":"109","month":"03","earnM":"40000","cost":"32000","profit":"8000","Gmargin":"25.0%"},
        {"year":"109","month":"04","earnM":"50000","cost":"38000","profit":"12000","Gmargin":"31.6%"},
        {"year":"109","month":"05","earnM":"60000","cost":"44000","profit":"16000","Gmargin":"36.4%"},
    ]
    return render(request, 'index.html', locals())

def charts(request):
    return render(request, 'charts.html')
def tables(request):
    earn_list=[
        {"year":"109","month":"01","earnM":"100000","cost":"68000","profit":"32000","Gmargin":"42.9%"},
        {"year":"109","month":"02","earnM":"80000","cost":"57000","profit":"23000","Gmargin":"40.4%"},
        {"year":"109","month":"03","earnM":"40000","cost":"32000","profit":"8000","Gmargin":"25.0%"},
        {"year":"109","month":"04","earnM":"50000","cost":"38000","profit":"12000","Gmargin":"31.6%"},
        {"year":"109","month":"05","earnM":"60000","cost":"44000","profit":"16000","Gmargin":"36.4%"},
    ]
    goodsout_list=[
        {"name":"手拿包","time":"2020-06-02","num":"10","price":"290","sum":"2900"},
        {"name":"後背包","time":"2020-06-08","num":"2","price":"490","sum":"980"},
        {"name":"郵差包","time":"2020-05-20","num":"4","price":"350","sum":"1400"},
        {"name":"錢包","time":"2020-04-30","num":"2","price":"500","sum":"1000"},
    ]
    goodsin_list=[
        {"name":"手拿包","time":"2020-06-01","num":"20","price":"100","sum":"2000"},
        {"name":"後背包","time":"2020-06-05","num":"5","price":"200","sum":"1000"},
    ]
    return render(request, 'tables.html', locals())
def error_404(request):
    return render(request, '404.html')
# def goods_in(request):
#     goodsin_list=[
#         {"name":"手拿包","time":"2020-06-01","num":"20","price":"100","sum":"2000"},
#         {"name":"後背包","time":"2020-06-05","num":"5","price":"200","sum":"1000"},
#     ]
#     return render(request, 'tables.html', locals())
# def goods_out(request):
#     goodsout_list=[
#         {"name":"手拿包","time":"2020-06-02","num":"10","price":"290","sum":"2900"},
#         {"name":"後背包","time":"2020-06-08","num":"2","price":"490","sum":"980"},
#         {"name":"郵差包","time":"2020-05-20","num":"4","price":"350","sum":"1400"},
#         {"name":"錢包","time":"2020-04-30","num":"2","price":"500","sum":"1000"},
#     ]
#     return render(request, 'tables.html', locals())
# def login(request):
#     return render(request, 'login.html')

def info_slide(request):
    return render(request, 'info_slide.html')


# 清單帶入值
def map_city(request, city_no = 0):
    city_list=[
        {"cityname":"宜蘭縣","citycode":"C10002"},
        {"cityname":"新竹縣","citycode":"C10004"},
        {"cityname":"苗栗縣","citycode":"C10005"},
        {"cityname":"彰化縣","citycode":"C10007"},
        {"cityname":"南投縣","citycode":"C10008"},
        {"cityname":"雲林縣","citycode":"C10009"},
        {"cityname":"嘉義縣","citycode":"C10010"},
        {"cityname":"屏東縣","citycode":"C10013"},
        {"cityname":"臺東縣","citycode":"C10014"},
        {"cityname":"花蓮縣","citycode":"C10015"},
        {"cityname":"澎湖縣","citycode":"C10016"},
        {"cityname":"基隆市","citycode":"C10017"},
        {"cityname":"新竹市","citycode":"C10018"},
        {"cityname":"嘉義市","citycode":"C10020"},
        {"cityname":"臺北市","citycode":"C63"},
        {"cityname":"高雄市","citycode":"C64"},
        {"cityname":"新北市","citycode":"C65"},
        {"cityname":"臺中市","citycode":"C66"},
        {"cityname":"臺南市","citycode":"C67"},
        {"cityname":"桃園市","citycode":"C68"},
        {"cityname":"連江縣","citycode":"C09007"},
        {"cityname":"金門縣","citycode":"C09020"},
    ]
    city = city_list[city_no]
    return render(request, 'map.html', locals())

def lack_ones(request, lack_no = 0): #低於庫存水位的商品清單
    lack_list=[
        {"name":"手拿包","num_now":"6","num_safe":"10"},
        {"name":"郵差包","num_now":"4","num_safe":"10"},
        {"name":"背包","num_now":"10","num_safe":"15"},
        {"name":"錢包","num_now":"2","num_safe":"5"},
        {"name":"托特包","num_now":"20","num_safe":"25"},
        {"name":"水桶包","num_now":"10","num_safe":"15"},
        {"name":"手提包","num_now":"15","num_safe":"30"},
    ]
    lackones = lack_list[lack_no]
    return render(request, 'index.html', locals())

def infoslide(request, info_no=0):
    info_list=[
        {"name":"Try-手拿包","type":"紅色","in_price":"100","out_price":"290","num_safe":"10","num_now":"6","storeAt":"A1"},
        {"name":"Try-手拿包","type":"白色","in_price":"100","out_price":"290","num_safe":"10","num_now":"6","storeAt":"A1"},
        {"name":"Try-手拿包","type":"黑色","in_price":"100","out_price":"290","num_safe":"10","num_now":"6","storeAt":"A1"},
        {"name":"Try-手拿包","type":"灰色","in_price":"100","out_price":"290","num_safe":"10","num_now":"6","storeAt":"A1"},
        {"name":"Try-郵差包","type":"紅色","in_price":"100","out_price":"350","num_safe":"10","num_now":"4","storeAt":"B3"},
        {"name":"Try-郵差包","type":"白色","in_price":"100","out_price":"350","num_safe":"10","num_now":"4","storeAt":"B3"},
        {"name":"Try-郵差包","type":"黑色","in_price":"100","out_price":"350","num_safe":"10","num_now":"4","storeAt":"B3"},
        {"name":"Try-錢包","type":"紅色","in_price":"100","out_price":"500","num_safe":"5","num_now":"2","storeAt":"C2"},
        {"name":"Try-錢包","type":"白色","in_price":"100","out_price":"500","num_safe":"5","num_now":"2","storeAt":"C2"},
        {"name":"Try-錢包","type":"黑色","in_price":"100","out_price":"500","num_safe":"5","num_now":"2","storeAt":"C2"},
    ]
    info_no = info_no
    info_goods = info_list[info_no]
    return render(request, 'info_slide.html', locals())
